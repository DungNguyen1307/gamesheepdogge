using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfController : MonoBehaviour
{
    public GameObject boom;

    public float minBoomTime = 2;

    public float maxBoomTime = 4;

    private float lastBoomTime = 0;
    private float boomTime = 0;

    private GameObject sheep;

    private Animator anim;

    public float throughBoomTime = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        UpdateBoomTime();
        sheep = GameObject.FindGameObjectWithTag("Player");
        anim = gameObject.GetComponent<Animator>();
        anim.SetTrigger("notBoom");
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= lastBoomTime + boomTime - throughBoomTime)
        {
            anim.SetTrigger("notBoom");
        }
        if (Time.time >= lastBoomTime + boomTime)
        {
            ThroughBoom();
        }
    }

    void UpdateBoomTime()
    {
        lastBoomTime = Time.time;
        boomTime = Random.Range(minBoomTime, maxBoomTime + 1);
    }

    private void ThroughBoom()
    {
        anim.SetTrigger("boom");
        GameObject bom = Instantiate(boom, transform.position, Quaternion.identity) as GameObject;
        bom.GetComponent<boomController>().target = sheep.transform.position;
        UpdateBoomTime();
        anim.SetTrigger("notBoom");
    }
}
 