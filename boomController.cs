using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boomController : MonoBehaviour
{
    public Vector3 target;
    public float moveSpeed = 5;
    public float DestroyTime = 2f;
    public GameObject ExPlor;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, DestroyTime);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate((transform.position - target) * moveSpeed * Time.deltaTime * -1);
    }

    void OnDestroy()
    {
      GameObject exp = Instantiate(ExPlor, transform.position, Quaternion.identity) as GameObject;
      Destroy(exp, 0.5f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            Destroy(gameObject);
        }
    }
}
